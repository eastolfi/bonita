package es.edu.android.bonita;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.Context;
import android.view.View.*;


public class MainActivity extends Activity {
	Context ctx;
	Button btMensaje;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		
		ctx = this;
		
		btMensaje = (Button) findViewById(R.id.btMensaje);
		btMensaje.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MensajesHelper.showMensaje(ctx);
			}
		});	
    }
	
	
}
