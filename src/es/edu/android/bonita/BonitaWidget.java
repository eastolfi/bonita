package es.edu.android.bonita;
import android.appwidget.*;
import android.content.*;
import android.widget.*;
import android.app.*;

public class BonitaWidget extends AppWidgetProvider{
	/*
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {

			
			
			RemoteViews views = new RemoteViews(context.getPackageName(),
												R.layout.widget);
			String test = String.valueOf(views.getLayoutId());
			Toast.makeText(context, test, Toast.LENGTH_SHORT).show();
			test = String.valueOf(R.id.widget);
			Toast.makeText(context, test, Toast.LENGTH_SHORT).show();
			
			
			
			Intent i = new Intent(context, MainActivity.class);
			if (i != null) {
				
				PendingIntent pendingIntent = PendingIntent.getActivity(
					context, 0, i, 0);
				views.setOnClickPendingIntent(R.id.widget, pendingIntent);
			}

			AppWidgetManager
				.getInstance(context)
				.updateAppWidget(
				intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS),
				views);
				
			
		}
	}
	
	public void onEnabled(Context context) {
		
	}
	*/
	
	private static final String SYNC_CLICKED    = "automaticWidgetSyncButtonClick";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews;
        ComponentName watchWidget;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
        watchWidget = new ComponentName(context, BonitaWidget.class);

        remoteViews.setOnClickPendingIntent(R.id.btTest, getPendingSelfIntent(context, SYNC_CLICKED));
        appWidgetManager.updateAppWidget(watchWidget, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        super.onReceive(context, intent);

        if (SYNC_CLICKED.equals(intent.getAction())) {

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            RemoteViews remoteViews;
            ComponentName watchWidget;

            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
            watchWidget = new ComponentName(context, BonitaWidget.class);

            //remoteViews.setTextViewText(R.id.btTest, "TESTING");
			//Toast.makeText(context, "binee", Toast.LENGTH_SHORT).show();
			MensajesHelper.showMensaje(context);

            appWidgetManager.updateAppWidget(watchWidget, remoteViews);

        }
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
}
