package es.edu.android.bonita;
import android.widget.*;
import android.content.*;

public class MensajesHelper {
	private static String prevMsg = null;
	
	private static final String[] mensajes = {
		"Te quiero ^^", "Te amo!", "Me encantas :)",
		"Siempre estaré contigo :)"
	};
	
	public static void showMensaje(Context ctx) {
		String texto = MensajesHelper.getMensaje();
		if (prevMsg != null) {
			while (texto.equals(prevMsg)) {
				texto = MensajesHelper.getMensaje();
			}
		}
		prevMsg = texto;
		Toast.makeText(ctx, texto, Toast.LENGTH_SHORT).show();
	}
	
	private static String getMensaje() {
		return mensajes[genRandom(0, mensajes.length)];
	}
	
	private static int genRandom(int min, int max) {
		int seed = (int) (Math.random() * (max - min));
		return min + seed;
	}
}
